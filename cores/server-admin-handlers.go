package cores

import (
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"net/http"
)

func (s *Server) RouteAdmin() http.Handler {
	//router for admin side
	r := chi.NewRouter()

	//admin middleware
	r.Use(middleware.Logger)
	r.Use(middleware.Recoverer)
	r.Use(middleware.StripSlashes)

	//routes admin
	r.Get("/", s.HandlerAdminRoot)
	r.Post("/", s.HandlerAdminRoot)

	return r
}
func (s *Server) HandlerAdminRoot(w http.ResponseWriter, r *http.Request) {
	_ = s.rend.Render("admin/index.html", nil, w)
}
