package cores

import (
	"gitlab.com/Spouk/gotool/config"

	"io"
	"io/fs"
	"log"
	"os"
	"sync"
)

var (
	logCorePrefix = "[core] "
)

type Core struct {
	Log *log.Logger
	Cfg *Config
	sw  *sync.WaitGroup
	//----server side
	Server *Server
}

func NewCore(configFile string) *Core {
	c := &Core{
		Log: log.New(os.Stdout, logCorePrefix, log.LstdFlags|log.Lshortfile),
		sw:  &sync.WaitGroup{},
	}
	//чтение конфига
	cr := config.NewConf("", c.Log.Writer())
	ccc := Config{}
	err := cr.ReadConfig(configFile, &ccc)
	if err != nil {
		c.Log.Println(err)
		os.Exit(2)
	}
	c.Cfg = &ccc

	//открытие файла-лога
	fh, err := c.OpenLogFile(c.Cfg.LogFile)
	if err != nil {
		c.Log.Fatal(err)
	}
	//определение множественный вывод на os.stdout + os.File
	mw := io.MultiWriter(os.Stdout, fh)
	c.Log.SetOutput(mw)

	//создание  сервера
	c.Server = NewServer(c.Log.Writer(), c)

	//возврат инстанса
	return c
}

func (c *Core) OpenLogFile(filename string) (*os.File, error) {
	c.Log.Printf("filename log: %s\n", filename)
	if stat, err := os.Stat(filename); err != nil {
		if err == os.ErrNotExist {
			fh, err := os.OpenFile(filename, os.O_CREATE|os.O_WRONLY, os.ModePerm)
			if err != nil {
				return nil, err
			}
			return fh, nil
		} else {
			if _, ok := err.(*fs.PathError); ok {
				fh, err := os.OpenFile(filename, os.O_CREATE|os.O_WRONLY, os.ModePerm)
				if err != nil {
					return nil, err
				}
				return fh, nil
			}
			return nil, err
		}
	} else {
		if !stat.IsDir() {
			fh, err := os.OpenFile(filename, os.O_APPEND|os.O_WRONLY, os.ModePerm)
			if err != nil {
				return nil, err
			}
			return fh, nil
		}
	}
	return nil, nil
}
