package cores

import "net/http"

//----------------------------------------------------------------------
// routes paths
//-----------------------------------------------------------------------
func (s *Server) Routes() {
	s.Mux.Get("/", s.HandlerRoot)
}

//---------------------------------------------------------------------
// handlers
//-----------------------------------------------------------------------

func (s *Server) HandlerRoot(w http.ResponseWriter, r *http.Request) {
	s.rend.Render("index.html", nil, w)
}
