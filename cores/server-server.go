package cores

import (
	"fmt"
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"gitlab.com/Spouk/gotool/render"
	"io"
	"log"
	"net/http"
	"os"
	"path/filepath"
)

var (
	logPrefix = "[web-server-mysql] "
)

type Server struct {
	Log  *log.Logger
	Mux  *chi.Mux
	c    *Core
	rend *render.Render
	db   *gorm.DB
}

func NewServer(l io.Writer, c *Core) *Server {
	s := &Server{
		Log: log.New(l, logPrefix, log.LstdFlags|log.Lshortfile),
		Mux: chi.NewRouter(),
		c:   c,
	}

	//миддлы
	s.Mux.Use(middleware.Logger)
	s.Mux.Use(middleware.Recoverer)
	s.Mux.Use(middleware.StripSlashes)

	////статичные файлы
	s.SetStaticRoute("/static", "/front/static")

	//render instance
	s.rend = render.NewRender(s.c.Cfg.Templatepath, s.c.Cfg.Templatedebug, s.c.Log, s.c.Cfg.Teamplatedebugfatal)

	//init routes
	s.Routes()

	//make database instance
	//d.User, d.Password, d.Host, d.Port, d.DatabaseName
	gorm.Open("mysql", fmt.Sprintf(dns, c.Cfg.))

	return s
}
func (s *Server) Run() {
	s.Log.Printf("starting server on `%v`\n", s.c.Cfg.Adresshttp)

	//http.Handle("/fuck/", http.StripPrefix("/fuck/", http.FileServer(http.Dir("/home/spouk/stock/s.develop/go/src/gitlab.com/web-server-mysql/front"))))
	//http.ListenAndServe(s.c.Cfg.Adresshttp, nil)
	s.Log.Fatal(http.ListenAndServe(s.c.Cfg.Adresshttp, s.Mux))
}
func (s *Server) RunTLS() {

}

//----------------------------------------------------------------------
// статичные файлы
//-----------------------------------------------------------------------
func (s *Server) SetStaticRoute(routePrefix string, realPathToFiles string) {
	s.Mux.Route(routePrefix, func(root chi.Router) {
		workDir, _ := os.Getwd()
		filesDir := filepath.Join(workDir, realPathToFiles)
		if s.c.Cfg.CoreDebug {
			s.Log.Printf("FilesDIR: %v\n", filesDir)
			s.Log.Printf("GETWD: %v\n", workDir)
		}
		fs := http.StripPrefix(routePrefix, http.FileServer(Dir(filesDir)))
		root.Get("/*", http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			fs.ServeHTTP(w, r)
		}))
	})
}

//реализация интерфейса http.FileSystem
type (
	MainerFS struct {
		fs http.FileSystem
	}
)

//реализация паттерна интерфейса FileSystem пакета http
func (m MainerFS) Open(name string) (http.File, error) {
	workDir, _ := os.Getwd()
	log.Printf("OPEN FILENAME: %v :`%v`\n", name, workDir)

	f, err := m.fs.Open(name)
	if err != nil {
		log.Printf("[ERROR] %v\n", err)
		return nil, err
	}
	fi, err := f.Stat()
	if err != nil {
		log.Printf("[ERROR] %v\n", err)
		return nil, err
	}
	if fi.IsDir() {
		log.Printf("IS DIR FU REQUEST....\n")
		return nil, os.ErrPermission
	}
	return f, nil
}
func Dir(path string) MainerFS {
	return MainerFS{fs: http.Dir(path)}
}
