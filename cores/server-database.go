package cores

import (
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

const logdbsprefix = "[dbs] "

func NewDatabaseConnector(c *Config) *Databases {

}

type Databases struct {
	gorm.DB
}

func (d *Databases) Query(s string) error {

}
