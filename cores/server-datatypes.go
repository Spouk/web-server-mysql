package cores

import (
	"github.com/jinzhu/gorm"
	"os"
	"time"
)

//d.User, d.Password, d.Host, d.Port, d.DatabaseName
const dns = "%s:%s@(%s:%s)/%s?charset=utf8&parseTime=True&loc=Local"

var tableList = []interface{}{
	ConfigTable{},
	Files{},
	User{},
	Post{},
	SessionTable{},
	Roles{},
	Logs{},
	Accessbit{},
	Linkref{},
}

type (
	Config struct {
		//core
		LogFile        string `yaml:"logfile"`
		ConfigPath     string `yaml:"configpath"`
		ConfigFile     string `yaml:"configfile"`
		ConfigDumpPath string `yaml:"config_dump_path"`
		ConfigDumpFile string `yaml:"config_dump_file"`
		CoreDebug      bool   `yaml:"coredebug"`
		//server
		Hostname            string   `yaml:"hostname"`
		Templatepath        string   `yaml:"templatepath"`
		Templatedebug       bool     `yaml:"templatedebug"`
		Teamplatedebugfatal bool     `yaml:"teamplatedebugfatal"`
		Readtimeout         int      `yaml:"readtimeout"`
		Writetimeout        int      `yaml:"writetimeout"`
		Contactreview       []string `yaml:"contactreview"`
		Adresshttp          string   `yaml:"adresshttp"`
		Certfile            string   `yaml:"certfile"`
		Keyfile             string   `yaml:"keyfile"`
		//roles
		Roles      []string `yaml:"roles"`
		RolesAdmin []string `yaml:"rolesadmin"`
		//database
		DBTypeDB             string `yaml:"dbtypedb"`
		DBHost               string `yaml:"dbhost"`
		DBPort               int    `yaml:"dbport"`
		DBUser               string `yaml:"dbuser"`
		DBPassword           string `yaml:"dbpassword"`
		DBDatabase           string `yaml:"dbdatabase"`
		DBSSLMode            bool   `yaml:"dbsslmode"`
		DBSetMaxIdleConns    int    `yaml:"dbsetmaxidleconns"`
		DBSetMaxOpenConns    int    `yaml:"dbsetmaxopenconns"`
		DBSetConnMaxLifetime int    `yaml:"dbsetconnmaxlifetime"`
		//project
		PaginateCountOnPage int      `yaml:"paginatecountonpage"`
		PaginateCountLinks  int      `yaml:"paginatecountlinks"`
		PaginateSortType    []string `yaml:"paginatesorttype"`
		PaginateDebug       bool     `yaml:"paginatedebug"`
		UploadPath          string   `yaml:"uploadpath"`
		SitemapPath         string   `yaml:"sitemappath"`
		SitemapHost         string   `yaml:"sitemaphost"`
		HostFullPathHTTP    string   `yaml:"hostfullpathhttp"`
		HostFullPathHTTPS   string   `yaml:"hostfullpathhttps"`
		SeoTitle            string   `yaml:"seotitle"`
		SeoDesc             string   `yaml:"seodesc"`
		SeoKeys             string   `yaml:"seokeys"`
		SeoRobot            string   `yaml:"seorobot"`
		LaterPostTimePeriod int      `yaml:"laterposttimeperiod"`
		//session
		MailTo               string        `yaml:"mailto"`
		MailFrom             string        `yaml:"mailfrom"`
		MailHost             string        `yaml:"mailhost"`
		MailPort             int           `yaml:"mailport"`
		MailUsername         string        `yaml:"mailusername"`
		MailPassword         string        `yaml:"mailpassword"`
		CSRFTimeActive       int           `yaml:"csrftimeactive"`
		CSRFSalt             string        `yaml:"csrfsalt"`
		CookieName           string        `yaml:"cookiename"`
		CookieDomain         string        `yaml:"cookiedomain"`
		CookieExpired        int64         `yaml:"cookieexpired"`
		CookieSalt           string        `yaml:"cookiesalt"`
		RoleDefaultUser      string        `yaml:"roledefaultuser"`
		SessionTime          time.Duration `yaml:"sessiontime"`
		SessionTimeExpired   time.Duration `yaml:"sessiontimeexpired"`
		SessionTimeSave      time.Duration `yaml:"sessiontimesave"`
		SessionPathSave      string        `yaml:"pathsavesession"`
		TimerTime            time.Duration `yaml:"timertime"`
		SleepTimeCatcher     time.Duration `yaml:"sleeptimecatcher"`
		DeferPostSleepTime   time.Duration `yaml:"deferpostsleeptime"`
		DeferPostTime        time.Duration `yaml:"deferposttime"`
		ContactReview        []string      `yaml:"contactreview"`
		FlashSalt            string        `yaml:"flashsatl"`
		DefaultUserCookie    string        `yaml:"defaultusercookie"`
		DefaultAdminEmail    string        `yaml:"defaultadminemail"`
		DefaultAdminPassword string        `yaml:"defaultadminpassword"`
		DefaultAdminRole     string        `yaml:"defaultadminrole"`
		RedisAdress          string        `yaml:"redisadress"`
		RedisDB              int           `yaml:"redisdb"`
		RedisPassword        string        `yaml:"redispassword"`
		DumpConfigFile       string        `yaml:"dumpconfigfile"`
	}

	//таблица с сессиями
	SessionTable struct {
		gorm.Model
		Cook        string `sql:"type:varchar(190); unique"` //cook = ID
		Active      bool
		Session     string `sql:"type:varchar(100000); "` //json формат Databox
		LastConnect time.Time
		//элементы структуры для веб-формы
		Errors map[string]string `gorm:"-"` //для проверки формы
	}

	//базовый конфиг , все изменные варианты хранить в Redis
	ConfigTable struct {
		gorm.Model
		Active bool
		Dump   string `sql:"type:varchar(100000); "`
		//элементы структуры для веб-формы
		Errors map[string]string `gorm:"-"` //для проверки формы
	}
	//конфиги, подписка, файлы
	Files struct {
		gorm.Model
		Name    string `sql:"type:varchar(190); unique"`
		Path    string `sql:"type:longtext"`
		Alt     string `sql:"type:longtext"`
		Title   string `sql:"type:longtext"`
		Size    int64
		Ext     string
		TagFile string `sql:"type:longtext"` //метка для файла
		//IDS
		UserID    uint //gorm.Model.ID
		PostID    uint //ID статьи //gorm.Model.ID
		ElementID uint //ID привязки к элементу интерфейса //gorm.Model.ID
		//элементы структуры для веб-формы
		Errors map[string]string `gorm:"-"` //для проверки формы
	}
	//roles
	Roles struct {
		gorm.Model
		Name string `sql:"type:varchar(200); unique"`
		//элементы структуры для веб-формы
		Errors map[string]string `gorm:"-"` //для проверки формы
	}
	//rights by role
	Accessbit struct {
		gorm.Model
		Roleid                          uint //role.id
		Read, Write, Delete, Update     bool
		Aread, Awrite, Adelete, Aupdate bool
		//элементы структуры для веб-формы
		Errors map[string]string `gorm:"-"` //для проверки формы
	}
	//logs
	Logs struct {
		gorm.Model
		Userid uint   //user.id
		Action string `sql:"type:longtext"`
		//элементы структуры для веб-формы
		Errors map[string]string `gorm:"-"` //для проверки формы
	}
	//для подсчета количества просмотров страница; Alias - post.alias
	Linkref struct {
		Userid uint
		Cookie string `sql:"type:varchar(200)"`
		Alias  string `sql:"type:varchar(200)"`
		//элементы структуры для веб-формы
		Errors map[string]string `gorm:"-"` //для проверки формы
	}
	//счетчики
	Counters struct {
		gorm.Model
		Alias string `sql:"type:varchar(200)"` //post.alias
		Views int    //counts viewers
		//элементы структуры для веб-формы
		Errors map[string]string `gorm:"-"` //для проверки формы
	}
	//пользователь тут хранить только основную информацию
	User struct {
		gorm.Model
		//table struct
		Login        string `sql:"type:varchar(200); unique"`
		Passwordhash string `sql:"type:varchar(500)"`
		Cookie       string `sql:"type:varchar(200)"`
		Connected    bool
		Logged       bool
		Roleid       uint //role.id
		OA2vkid      uint
		OA2googleid  uint
		OA2yandexid  uint
		Lastconnect  time.Time
		Remoteaddr   string `sql:"type:varchar(200)"`
		Useragent    string `sql:"type:varchar(200)"`
		Email        string `sql:"type:varchar(190); unique"`
		Avatar       uint   //image.ID картинки на лого ; file.ID = [UINT]
		Username     string
		//temp data struct
		Role string
		//элементы структуры для веб-формы
		Errors map[string]string `gorm:"-"` //для проверки формы
		Robot  bool              `gorm:"-"`
	}
	//статьи
	Post struct {
		//table
		gorm.Model
		Tagid         uint      //gorm.Model.ID
		Authorid      uint      //gorm.Model.ID
		Title         string    `sql:"type:LONGTEXT"`
		Alias         string    `sql:"type:varchar(190); unique"`
		Body          string    `sql:"type:LONGTEXT"`
		PreBody       string    `sql:"type:LONGTEXT"`
		MetaKeys      string    `sql:"type:LONGTEXT"`
		MetaDesc      string    `sql:"type:LONGTEXT"`
		MetaRobot     string    `sql:"type:LONGTEXT"`
		MetaTitle     string    `sql:"type:LONGTEXT"`
		FileImagePost uint      `sql:"type:int unsigned"` //FileImagePost string    //(ID) (path) файла-картинки, который используется для отображения в списке статей основной картинки
		LaterPost     bool      //флаг отложенной публикации
		LaterData     time.Time //дата когда осуществить публикацию:: time.unix:time.unix
		Active        bool      //активна ли публикация
		//in struct adding data
		Files         []Files           //список других картинок и файлов, связанных с этой публикацией
		Errors        map[string]string `gorm:"-"` //для проверки формы
		LaterTimeForm string            `gorm:"-"` // поле для формы input.Time
		LaterDataForm string            `gorm:"-"` //полe для формы input.Data
	}
	//метки=категории
	Tag struct {
		gorm.Model
		Name      string `sql:"type:varchar(200); unique"`
		Desc      string `sql:"type:varchar(500);"`
		Countpost int
		Errors    map[string]string `gorm:"-"` //для проверки формы
	}

	//рекламные блоки
	Advertise struct {
		gorm.Model
		Tag    string            `sql:"type:varchar(190); unique"` //уникальная метка, за эту метку функция будет вытаскивать блок
		Type   string            // yandex, google, other
		Data   string            //js+html+css код блока для вставки на странице
		Errors map[string]string `gorm:"-"` //для проверки формы
	}
	//----------------------------------------------------------------------
	// конфигурация
	//-----------------------------------------------------------------------
	ConfigStruct struct {
		gorm.Model
		//global -> статичный сегмент
		TemplatePath          string        `yaml:"templatepath"`
		TemplateDebug         bool          `yaml:"templatedebug"`
		TemplateDebugFatal    bool          `yaml:"teamplatedebugfatal"`
		AdressHTTP            string        `yaml:"adresshttp"`
		ReadTimeout           time.Duration `yaml:"readtimeout"`
		WriteTimeout          time.Duration `yaml:"writetimeout"`
		CertFile              string        `yaml:"certfile"`
		KeyFile               string        `yaml:"keyfile"`
		RedirectTrailingSlash bool          `yaml:"redirecttrailingslash"`
		RedirectFixedPath     bool          `yaml:"redirectfixedpath"`
		//Logging -> статичный сегмент
		Logfile        string        `yaml:"logfile"`
		LogTagsyslog   string        `yaml:"logtagsyslog"`
		LogPrefix      string        `yaml:"logprefix"`
		StaticPath     string        `yaml:"staticfile"`
		StaticPrefix   string        `yaml:"staticprefix"`
		FileTimerSleep time.Duration `yaml:"filetimersleep"`
		//roles
		Roles      []string `yaml:"roles"`
		RolesAdmin []string `yaml:"rolesadmin"`

		//database sqlite (hdd + ramfs)
		DBSQLiteHDD         string        `yaml:"dbsqlitehdd"`
		DBSQLiteRAM         string        `yaml:"dbsqliteram"`
		DBSqliteTimerBackup time.Duration `yaml:"dbsqlitetimer"`

		//sitemap + default category + post links
		Hostname        string `yaml:"hostname"`
		DefaultCategory string `yaml:"defaultcategory"`

		//database mysql
		DBTypeDB             string `yaml:"dbtypedb"`
		DBHost               string `yaml:"dbhost"`
		DBPort               string `yaml:"dbport"`
		DBUser               string `yaml:"dbuser"`
		DBPassword           string `yaml:"dbpassword"`
		DBDatabase           string `yaml:"dbdatabase"`
		DBSSLMode            bool   `yaml:"dbsslmode"`
		DBSetMaxIdleConns    int    `yaml:"dbsetmaxidleconns"`
		DBSetMaxOpenConns    int    `yaml:"dbsetmaxopenconns"`
		DBSetConnMaxLifetime int    `yaml:"dbsetconnmaxlifetime"`
		//project
		PaginateCountOnPage int           `yaml:"paginatecountonpage"`
		PaginateCountLinks  int           `yaml:"paginatecountlinks"`
		PaginateSortType    []string      `yaml:"paginatesorttype"`
		PaginateDebug       bool          `yaml:"paginatedebug"`
		UploadPath          string        `yaml:"uploadpath"`
		SitemapPath         string        `yaml:"sitemappath"`
		SitemapHost         string        `yaml:"sitemaphost"`
		HostFullPathHTTP    string        `yaml:"hostfullpathhttp"`
		HostFullPathHTTPS   string        `yaml:"hostfullpathhttps"`
		SeoTitle            string        `yaml:"seotitle"`
		SeoDesc             string        `yaml:"seodesc"`
		SeoKeys             string        `yaml:"seokeys"`
		SeoRobot            string        `yaml:"seorobot"`
		LaterPostTimePeriod time.Duration `yaml:"laterposttimeperiod"`
		//session
		MailTo         string `yaml:"mailto"`
		MailFrom       string `yaml:"mailfrom"`
		MailHost       string `yaml:"mailhost"`
		MailPort       int    `yaml:"mailport"`
		MailUsername   string `yaml:"mailusername"`
		MailPassword   string `yaml:"mailpassword"`
		CSRFTimeActive int    `yaml:"csrftimeactive"`
		CSRFSalt       string `yaml:"csrfsalt"`
		//Cookie part
		CookieName             string        `yaml:"cookiename"`
		CookieDomain           string        `yaml:"cookiedomain"`
		CookieExpired          int64         `yaml:"cookieexpired"`
		CookieSalt             string        `yaml:"cookiesalt"`
		CookieAnonymous        string        `yaml:"cookieanonymous"`
		CookieRegister         string        `yaml:"cookieregister"`
		RoleDefaultUser        string        `yaml:"roledefaultuser"`
		SessionTime            time.Duration `yaml:"sessiontime"`
		SessionTimeExpired     time.Duration `yaml:"sessiontimeexpired"`
		SessionTimeSave        time.Duration `yaml:"sessiontimesave"`
		SessionPathSave        string        `yaml:"pathsavesession"`
		SessionTimeSleepWorker time.Duration `yaml:"sessiontimesleepworker"`
		TimerTime              time.Duration `yaml:"timertime"`
		SleepTimeCatcher       time.Duration `yaml:"sleeptimecatcher"`
		DeferPostSleepTime     time.Duration `yaml:"deferpostsleeptime"`
		DeferPostTime          time.Duration `yaml:"deferposttime"`
		ContactReview          []string      `yaml:"contactreview"`
		FlashSalt              string        `yaml:"flashsatl"`
		RedisAdress            string        `yaml:"redisadress"`
		RedisDB                int           `yaml:"redisdb"`
		RedisPassword          string        `yaml:"redispassword"`
		DumpConfigFile         string        `yaml:"dumpconfigfile"`
		//oauth2 authorization
		// +vk+
		O2vkid       string   `yaml:"o2vkid"`
		O2vkkey      string   `yaml:"o2vkkey"`
		O2vkcallback string   `yaml:"o2vkcallback"`
		o2vkscope    []string `yaml:"o2vkscope"`
		// +yandex+
		O2yandexid       string   `yaml:"o2yandexid"`
		O2yandexkey      string   `yaml:"o2yandexkey"`
		O2yandexcallback string   `yaml:"o2yandexcallback"`
		o2yandexscope    []string `yaml:"o2yandexscope"`
		// +google+
		O2googleid       string   `yaml:"o2googleid"`
		O2googlekey      string   `yaml:"o2googlekey"`
		O2googlecallback string   `yaml:"o2googlecallback"`
		O2googlescope    []string `yaml"o2googlescope"`

		//defaultusers
		DefaultUsers []User            `yaml:"defaultusers"`
		Errors       map[string]string `gorm:"-"` //для проверки формы
	}
	//структура для загрузки файла
	FileInfo struct {
		//ошибка  есть сообщение
		Error     bool
		Msg       string //текст сообщения/ошибки/вопроса
		ErrorType int    //тип ошибки, 0 - серверная, 1 - запрос на подтверждение чего-либо,

		//перезаписывать ли файл если он найден на серверной части
		RewriterFile bool //по умолчанию false

		//флаг, показывающий, что файл успешно передан и файловый дескриптер закрыт
		SuccessLoadFile bool

		//сведения от клиента
		FileID       string //уникальный идентификатор файла
		Name         string //имя файла
		Size         int64  //размер файла
		TotalChunk   int64  //общее количество чанков
		ChunkSize    int64  //размер чанков
		CurrentChunk int64  //текущий номер чанка который содержит FormData полученная от ajax

		//информация для серверной части
		fileHandler          *os.File    //файловый дескриптор, куда пишутся чанки
		countGetChunkSuccess []int64     //количество полученных чанков и успешно записанных
		countGetChunkError   []int64     //количество полученных чанков с ошибкой, чтобы можно было повторно из запросить
		activeUpload         bool        //активна ли в настоящий момент загрузка
		timer                *time.Timer //таймер, который срабатывает по истечении заданного периода

		//элементы структуры для веб-формы
		Errors map[string]string `gorm:"-"` //для проверки формы
	}
)
