package main

import (
	"flag"
	"fmt"
	"gitlab.com/web-server-mysql/cores"
	"os"
)

var (
	configFile string
)

func main() {
	cfg := "/home/spouk/stock/s.develop/go/src/gitlab.com/web-server-mysql/config/config.yaml"
	core := cores.NewCore(cfg)
	fmt.Printf("%v\n", core)
	core.Server.Run()

	os.Exit(2)

	flag.StringVar(&configFile, "config", "", "--config `configfn.yaml`")
	flag.Parse()

	fmt.Printf("%v  %v\n", len(os.Args), configFile)

	if len(os.Args) == 3 {
		fmt.Printf("%v\n", len(os.Args))
		core := cores.NewCore(configFile)
		core.Server.Run()
		fmt.Printf("%v\n", core)

	} else {
		flag.Usage()
		os.Exit(1)
	}

}
