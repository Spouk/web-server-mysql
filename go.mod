module gitlab.com/web-server-mysql

go 1.16

require (
	github.com/go-chi/chi/v5 v5.0.3
	github.com/go-yaml/yaml v2.1.0+incompatible // indirect
	github.com/jinzhu/gorm v1.9.16
	gitlab.com/Spouk/gotool v0.0.0-20200529055947-6593d2f8460f
	gopkg.in/check.v1 v1.0.0-20201130134442-10cb98267c6c // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
)
